const http = require('http');
const fs = require('fs');

console.log('Getting data from http://www.unicode.org/Public/UCD/latest/ucd/UnicodeData.txt');

http.get('http://www.unicode.org/Public/UCD/latest/ucd/UnicodeData.txt', (res) => {
    if (res.statusCode !== 200) {
        console.error('Failed to get Unicode database. ');
        console.error(res.statusMessage);
        process.exit(-1);
        return;
    }

    let data = '';
    res.on('data', (chunk) => { data += chunk; });
    res.on('end', () => {
        console.log('Download complete');

        console.log('Processing');

        let entries = data.split('\n').map(a => a.split(';'));

        /*entries = entries.map(entry => {
            return {
                codepoint: entry[0],
                name: entry[1],
                category: entry[2],
                combiningClass: entry[3],
                bidiClass: entry[4],
                decompositionType: entry[5],
                numericType: entry[6],
                bidiMirrored: entry[7],
                unicode1Name: entry[8],
                isoComment: entry[9],
                uppercaseMapping: entry[10],
                lowercaseMapping: entry[11],
                titlecaseMapping: entry[12]
            };
        });*/

        let categories = [];

        entries.forEach(entry => {
            if (entry[2] && !categories.includes(entry[2])) {
                categories.push(entry[2]);
            }
        });

        let outputData = 'module.exports.chars = [';

        outputData += entries.map(entry =>
            {
                entry = entry.map(a =>
                    '\'' + a.replace('\n', '\\n')
                            .replace('\r', '\\r')
                            .replace('\'', '\\\'')
                            .replace('\\', '\\\\') + '\''
                );
                return `[${ entry.join(',') }]`;
            }
        ).join(',');

        outputData += '];module.exports.categories = [';

        outputData += categories.map(category =>
            {
                return '\'' + 
                    category.replace('\n', '\\n')
                            .replace('\r', '\\r')
                            .replace('\'', '\\\'')
                            .replace('\\', '\\\\') + '\'';
            }
        ).join(',');

        outputData += '];'

        fs.writeFileSync('./data.gen.js', outputData);

        console.log('done!');
    });
});