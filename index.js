var data = require('./data.gen.js');

module.exports.characters = {};

data.chars.forEach(char => {
    module.exports.characters[char[0].toLowerCase()] = {
        name: char[1],
        category: char[2],
        combiningClass: char[3],
        bidiClass: char[4],
        decompositionType: char[5],
        numericType: char[6],
        bidiMirrored: char[7],
        unicode1Name: char[8],
        isoComment: char[9],
        uppercaseMapping: char[10],
        lowercaseMapping: char[11],
        titlecaseMapping: char[12]
    };
});

module.exports.categories = {};

data.categories.forEach(category => {
    module.exports.categories[category] = {
        isInCategory: function(c) {
            let charInfo = module.exports.findCharacter(c);
            return charInfo && charInfo.category == category;
        }
    };
});

module.exports.findCharacter = function(c) {
    if (c.length < 1) return null;
    let codePoint = c.codePointAt(0).toString(16);
    while (codePoint.length < 4) { codePoint = '0' + codePoint; }
    return module.exports.characters[codePoint];
}