# Unicode helper

A Unicode database for javascript, compiled from http://www.unicode.org/Public/UCD/latest/ucd/UnicodeData.txt on install.

## install

```
npm install unicode-helper
```

## example

```javascript
> const unicode = require('unicode-helper');
{ characters: [Object],
  categories: [Object],
  findCharacter: [Function] }
> unicode.findCharacter('😀');
{ name: 'GRINNING FACE',
  category: 'So',
  combiningClass: '0',
  bidiClass: 'ON',
  decompositionType: '',
  numericType: '',
  bidiMirrored: '',
  unicode1Name: '',
  isoComment: 'N',
  uppercaseMapping: '',
  lowercaseMapping: '',
  titlecaseMapping: '' }
```