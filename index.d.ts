export declare interface UnicodeCharacterInfo {
    name: string;
    category: string,
    combiningClass: string,
    bidiClass: string,
    decompositionType: string,
    numericType: string,
    bidiMirrored: string,
    unicode1Name: string,
    isoComment: string,
    uppercaseMapping: string,
    lowercaseMapping: string,
    titlecaseMapping: string
}

export declare const characters: {
    [index: string]: UnicodeCharacterInfo
};

export declare const category: {
    [index: string]: {
        isInCategory(c: string): boolean;
    }
};

export declare function findCharacter(c: string): UnicodeCharacterInfo | null;